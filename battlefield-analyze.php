<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
h1{
        text-align: center;
}
</style>
</head>
<body><div id="main">
 
<h1>Battlefield Analysis</h1>
<h2>Latest Critiques</h2>

<?php
    $mysqli = new mysqli("localhost", "wustl_inst", "wustl_pass", "battlefield");
    // Check connection
    if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
    }
    $stmt = $mysqli->prepare("SELECT critique FROM reports");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->execute();
    $stmt->bind_result($critique);
    echo "<ul>\n";
    while($stmt->fetch()){
	printf("\t<li>%s %s</li>\n",
		htmlspecialchars($critique)
	);
    }
    echo "</ul>\n";
    $stmt->close();
?>

<h2>Battlefield Statistics</h2>

<?php
    $mysqli = new mysqli("localhost", "wustl_inst", "wustl_pass", "battlefield");
    // Check connection
    if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
    }
    $stmt = $mysqli->prepare("SELECT ammunition, duration FROM reports order by soldiers");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->execute();
    $stmt->bind_result($ammo, $dur);
    echo "<ul>\n";
    while($stmt->fetch()){
        $avg = $ammo/$dur;
	printf($avg
	);
    }
    echo "</ul>\n";
    $stmt->close();
?>

<a href="/~samshapiro/battlefield-submit.html">Submit a New Battle Report</a>
 
</div></body>
</html>
<?php
session_start();
if(isset($_POST['Submit']) ){
    $ammo = sprintf("%u\n", $_POST['ammo']);
    $soldiers = sprintf("%u\n", $_POST['soldiers']);
    $duration = sprintf("%f\n", $_POST['duration']);
    $critique = htmlentities($_POST['critique']);

    $mysqli = new mysqli("localhost", "wustl_inst", "wustl_pass", "battlefield");
    // Check connection
    if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
    }
    
    $stmt = $mysqli->prepare("insert into reports (ammunition, soldiers, duration, critique) values (?, ?, ?, ?)");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
    }
    $stmt->bind_param('iids', $ammo, $soldiers, $duration, $critique);
    $stmt->execute();
    $stmt->close();
    $mysqli->close();
    header( 'Location: /~samshapiro/battlefield-submit.html' ) ; 
}
else{
    echo 'Error: no POST variables present.';
}
?>
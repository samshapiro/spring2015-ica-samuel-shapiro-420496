samshapiro@ip-172-31-51-45:~$ mysql -u wustl_inst -p
Enter password: wustl_pass
CREATE DATABASE battlefield;
USE battlefield;
CREATE TABLE reports(
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    ammunition SMALLINT UNSIGNED NOT NULL,
    soldiers SMALLINT UNSIGNED NOT NULL,
    duration DOUBLE(6,1) UNSIGNED NOT NULL,
    critique TINYTEXT,
    posted TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
    ) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
